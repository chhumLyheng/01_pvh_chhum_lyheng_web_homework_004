import React, { PureComponent } from 'react'
import 'flowbite';

export class Table extends PureComponent {
    render() {
        return (
            <div class=" flex justify-center">

                <div class=" mt-8 relative overflow-x-auto shadow-md sm:rounded-lg ">
                    <table class=" text-sm text-center text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    ID
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    EMAIL
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    USERNAME
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    AGE
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    ACTION
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-gray-100 text-gray-700 ">
                            {this.props.data.map((tables) => (
                                <tr>
                                    <td>{tables.id}</td>
                                    <td>{tables.email}</td>
                                    <td>{tables.useName}</td>
                                    <td>{tables.age}</td>
                                    <td ><button type="button" class="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">pandding</button>
                                        <button type="button" class="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Show More</button></td>
                                </tr>
                            ))}
                        </tbody>
                        {/* <butto n onClick={tables.status}></butto><button onClick={tables.status}></button> */}

                    </table>
                </div>
            </div>
        )
    }
}


export default Table