import React, { PureComponent } from 'react'
import Table from './Table'

export class Input extends PureComponent {
  constructor() {
    super()
    this.state = {
      personInform: [
        { id: 1, email: "lyhengchhum@gmail.com", userName: "Chhum lyheng", age: "22", status: "Pending" },
        { id: 2, email: "lyhearchhum@gmail.com", userName: "Chhum lyhear", age: "20", status: "Pending" },
        { id: 3, email: "mariochhum@gmail.com", userName: "Chhum mario", age: "15", status: "Pending" },
      ],


    }
  }

  onRegister = () => {
    const register = { id: this.state.personInform.length + 1, email: document.getElementById("inputEmail").value, userName: document.getElementById("inputUsername").value, age: document.getElementById("inputAge").value, status: "Pendding" }
    this.setState({
      personInform: [...this.state.personInform, register],
    })

  }




  render() {
    return (
      <div >
        {/* Please fill your information*/}

        <div>
          <h1 className="text-4xl font-bold text-center m-8 ">
            <span class="font-extrabold text-transparent text-4xl bg-clip-text bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500">Please fill your </span>
            <span class="font-extrabold">information</span>
          </h1>
        </div>


        {/* inputEmail */}
        <div class="flex justify-center w-9/14">
          <div class="  ">
            <label for="inputEmail" class=" block mb-2 text-sm font-medium text-gray-900 dark:text-white ">Your Email</label>
            <div class="relative mb-6 ">
              <div class=" absolute inset-y-0 left-0  px-3 inline-flex items-center pl-3 pointer-events-none">
                <svg aria-hidden="true" class="w-5 h-5  text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
              </div>
              <input type="text" id="inputEmail" class=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block  pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@Email.com" />
            </div>

            {/* inputUsername */}

            <label for="inputUsername" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
            <div class="flex ">
              <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                @
              </span>
              <input type="text" id="inputUsername" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0  text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="user name" />
            </div>

            {/* inputAge */}
            <label for="inputAge" class="mtg-8 block mb-2 text-sm font-medium text-gray-900 dark:text-white">Age</label>
            <div class="flex ">
              <span class=" inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              </span>
              <input type="text" id="inputAge" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="age" />
            </div>
            </div>
        </div>
            {/* Register */}
            <div class="flex justify-center">

              <button type="submit" onClick={this.onRegister} class="mt-8  text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Register</button>
            </div>

        
        {/* Table Display */}
        <Table data={this.state.personInform} />

      </div>
    )
  }
}

export default Input